package com.mpr.orderManager.domain;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.mpr.orderManager.service.ClientDetailsManager;

public class ClientDetailsManagerTest {
	ClientDetailsManager cdManager = new ClientDetailsManager();
	
	@Test
	public void checkConnection(){
		assertNotNull(cdManager.getConnection());
	}
}
