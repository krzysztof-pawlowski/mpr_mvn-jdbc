package com.mpr.orderManager.domain;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.mpr.orderManager.service.AddressManager;

public class AddressManagerTest {
	AddressManager aManager = new AddressManager();
	
	private final static String STREET = "Brzegi";
	private final static String BUILDING = "55";
	private final static String FLAT = "209";
	private final static String POSTAL = "80-041";
	private final static String CITY = "Gdansk";
	private final static String COUNTRY = "Polska";
	
	@Test
	public void checkConnection(){
		assertNotNull(aManager.getConnection());
	}
	
	@Test
	public void checkAdding() {
		Address address = new Address(STREET, BUILDING, FLAT, POSTAL, CITY, COUNTRY);
		
		aManager.clearAddresses();
		assertEquals(1, aManager.addAddress(address));
		
		List<Address> addresses = aManager.getAllAddresses();
		Address addressRetrieved = addresses.get(0);
		
		assertEquals(STREET, addressRetrieved.getStreet());
		assertEquals(BUILDING, addressRetrieved.getBuildingNum());
	}
}
